var uuid = require('node-uuid');
var fs = require('fs');
var Db = require('mongodb').Db
  , Connection = require('mongodb').Connection
  , Server = require('mongodb').Server
  , GridStore = require('mongodb').GridStore
  , format = require('util').format;
// var MongoClient = mongodb.MongoClient;
var db =  new Db('fileDB', new Server("127.0.0.1", 27017, {auto_reconnect: true, poolSize: 5}));

var upload;

exports.putFileToGridfs = function(req, callback) {
  var source = req.files.qqfile.path;
  var fileid = req.body.qquuid;
  var gs = new GridStore(db, fileid, "w", {'content_type':req.files.qqfile.type,
    'metadata': {'origname' : req.files.qqfile.name}});
  gs.open(function(err, gridStore) {
    gridStore.writeFile(source, function(err) {
      if (!err) {
        fs.unlinkSync(source);
        callback({success: true, fileid: fileid});
      }
    });
  });
};

exports.removeFileFromGridfs = function(fileid, callback) {
  var gs = new GridStore(db, fileid, "w");
  gs.open(function(err, gridStore) {
    gridStore.unlink(function(err) {
      if (!err) {
        callback({success: true});
      }
    });
  });
};

exports.getFileMimeType = function(fileid, res) {
  var gs = new GridStore(db, fileid, "r");
  gs.open(function(err, gs) {
    res.header('Content-type', gs.contentType);
    res.end();
  });
}

exports.readFileFromGridfs = function (fileid, res) {
  var gs = new GridStore(db, fileid, "r");
  gs.open(function(err, gs) {
    res.header('Content-type', gs.contentType);
    gs.pipe(res);
  })
};

// Main function to receive and process the file upload data asynchronously
exports.uploadFile = function(req, callback) {

    // Moves the uploaded file from temp directory to it's destination
    // and calls the callback with the JSON-data that could be returned.
    var moveToDestination = function(sourcefile) {
        moveFile(sourcefile, function(err) {
            if(!err)
                callback({success: true});
            else
                callback({success: false, error: err});
        });
    };

    // Direct async xhr stream data upload, yeah baby.
    if(req.xhr) {
        // var fname = url.parse(req.url, true).query.qqfile;

        // Be sure you can write to '/tmp/'
        var tmpfile = '/tmp/'+uuid.v1();
        console.log(tmpfile);
        // Open a temporary writestream
        var ws = fs.createWriteStream(tmpfile);
        ws.on('error', function(err) {
            console.log("uploadFile() - req.xhr - could not open writestream.");
            callback({success: false, error: "Sorry, could not open writestream."});
        });
        ws.on('close', function(err) {
            moveToDestination(tmpfile);
        });

        // Writing filedata into writestream
        req.on('data', function(data) {
            ws.write(data);
            console.log('data arriving');
        });
        req.on('end', function() {
            ws.end();
            console.log('req end');
        });
    }
    // Old form-based upload
    else {
        moveToDestination(req.files.qqfile.path, targetdir+req.files.qqfile.name);
    }
};

// Moves a file asynchronously over partition borders
var moveFile = function(source, callback) {
  var gs = new GridStore(db, source, "w");
  gs.open(function(err, gridStore) {

    gridStore.writeFile(source, function(err) {
      if (!err) {
        fs.unlinkSync(source);
        callback();
      }
    });
  });
};