
/*
 * GET home page.
 */
var fileupload = require('../fileupload');
var uuid = require('node-uuid');
var fs = require('fs');
var url = require('url');

var settings = {
    node_port: process.argv[2] || 3000,
//    uploadpath: __dirname + '/uploads/'
    uploadpath: '/tmp'
};

module.exports = function(app) {
  app.get('/', function(req, res) {
    res.render('index', {
      title: '首页'
    });
  });
  app.get('/node/:id', function(req, res) {
    res.render('index', {
      title: 'resource'
    })
  });
  app.get('/addnode', function(req, res) {
    res.render('index', {
      title: '添加内容'
    })
  });
  app.get('/listNodes', function(req, res) {
    res.render('index', {
      title: '浏览内容'
    })
  });
  app.get('/bs', function(req, res) {
    res.render('index2', {
      title: 'bs'
    })
  });
  app.post('/upload/receiver', function(req, res) {
    fileupload.putFileToGridfs(req, function(data) {
        if(data.success)
            res.send(JSON.stringify(data), {'Content-Type': 'text/plain'}, 200);
        else
            res.send(JSON.stringify(data), {'Content-Type': 'text/plain'}, 404);
    });
  });
  app.delete('/upload/receiver/:id', function(req, res) {
    fileupload.removeFileFromGridfs(req.params.id, function(data) {
      if(data.success)
        res.send(JSON.stringify(data), {'Content-Type': 'text/plain'}, 200);
      else
        res.send(JSON.stringify(data), {'Content-Type': 'text/plain'}, 404);
    });
  });
  app.get('/upload/receiver/:id', function(req, res) {
    fileupload.readFileFromGridfs(req.params.id, res);
  });
  app.get('/upload/receiver/:id/mimetype', function(req, res) {
    fileupload.getFileMimeType(req.params.id, res);
  })
  return app.router;
};