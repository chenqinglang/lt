'use strict';

/* Services */

// Demonstrate how to register services
// In this case it is a simple value service.
angular.module('myApp.services', []).
  value('version', '0.1').
  value('uploadpath', 'http://localhost:3000/upload/receiver/').
  value('basepath', 'http://localhost:3000/node/').
  value('nodeurl', 'http://localhost:7474/db/data/node/').
  value('relationshipurl', 'http://localhost:7474/db/data/relationship/').
  value('cypherurl', 'http://localhost:7474/db/data/cypher').
  value('richtextkey', 'richtextw2j5a8l7');

angular.module('myApp.Node', ['neo4jServices', 'myApp.services']).
  factory('Node', function(Relationship, nodeurl, Utility, $http) {
    var Node = function(data) {
      if (data) {
        this.data = data;
        this.properties = data.data;
        this.id = Utility.extractid(data.self);
      }
    }

    Node.getById = function(id) {
      var node = new Node();
      $http.get(nodeurl + id).success(function(data) {
        node.data = data;
        node.properties = data.data;
        node.id = Utility.extractid(node.data.self);
        node.getRelationships();
      });
      return node;
    }

    Node.create = function(label, defered) {
      var node = new Node();
      $http.post(nodeurl, {label: label}).success(function(data) {
        node.id = Utility.extractid(data.self);
        defered.resolve();
      });
      return node;
    }

    Node.prototype.delete = function() {
      // First delete all relationships with this node
      this.relationships.forEach(function(x) {
        $http.delete(x.data.self);
      })
      $http.delete(this.data.self);
    }

    Node.prototype.getRelationships = function() {
      var node = this;
      node.relationships = [];
      $http.get(this.data.all_relationships).success(function(data) {
        data.forEach(function(x,i) {
          var relationship = new Relationship(x);
          node.relationships.push(relationship);
        });
      });
    }

    Node.prototype.updateProperty = function(key, value) {
      this.properties[key] = value;
      $http.put(this.data.property.replace(/{key}/, key), '"'+ value +'"');
    }

    Node.prototype.addProperty = function(key, value) {
      this.properties[key] = value;
      if (typeof value == 'string') {
        $http.put(this.data.property.replace(/{key}/, key), '"'+ value +'"');
      }
      else {
        $http.put(this.data.property.replace(/{key}/, key), value);
      }
    }

    Node.prototype.delProperty = function(key) {
      delete this.properties[key];
      $http.delete(this.data.property.replace(/{key}/, key));
    }

    Node.prototype.addRelationship = function(to, type) {
      var node = this;
      $http.post(this.data.create_relationship, {to: to, type: type}).success(function(data) {
        var relationship = new Relationship(data);
        node.relationships.push(relationship);
      });
    }

    Node.prototype.deleteRelationship = function(relationship) {
      relationship.delete();
    }

    return Node;
  }).
  factory('Relationship', function($http) {
    function Relationship(data) {
      var relationship = this;
      this.data = data;
      this.properties = data.data;
      $http.get(data.end +'/properties/label').success(function(data) {
        relationship.data.endlabel = data;
      });
      $http.get(data.start +'/properties/label').success(function(data) {
        relationship.data.startlabel = data;
      });
    }

    Relationship.prototype.delete = function() {
      $http.delete(this.data.self);
    }

    Relationship.prototype.updateProperty = function(key, value) {
      this.properties[key] = value;
      $http.put(this.data.property.replace(/{key}/, key), '"'+ value +'"');
    }

    Relationship.prototype.delProperty = function(key) {
      delete this.properties[key];
      $http.delete(this.data.property.replace(/{key}/, key));
    }

    return Relationship;
  }).
  factory('Utility', function(Cypher, Index, basepath) {
    return {
      getrelationshipstypes : function(defered) {
        Cypher.search({
          "query" : "start x  = node(*), n = node(*) match x-[r]->n return distinct TYPE(r)"
        }, function(data) {
          var results = data.data.map(function(x, i) {
            return {id: i, text: (x[0] == null) ? "" : x[0]};
          });
          defered.resolve(results);
        });
      },
      getpropertytypes : function() {
        return {
        }
      },
      extractid: function(url) {
        var slashposition = url.lastIndexOf('/');
        return url.substring(slashposition+1);
      },

      createpathwithid: function(id) {
        return basepath + id;
      },

      searchnodebylabel : function(query) {
        var u = this;
        Index.search({query: 'label:' + "*" + query.term + "*"}, function(data) {
          var result = data.map(function(x, i) {
            return {id:u.extractid(x.self), text:x.data.label};
          });
          query.callback({results: result})
        });
      }
    };
  });


angular.module('neo4jServices', ['ngResource']).
  factory("Cypher", function($resource){
    return $resource('http://localhost\\:7474/db/data/cypher', {}, {
      search: {method: 'POST'}
    });
  }).
  factory("Index", function($resource) {
    return $resource('http://localhost\\:7474/db/data/index/auto/node\\/', {}, {
      search: {method: 'GET', isArray:true}
    });
  });
