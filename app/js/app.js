'use strict';

// Declare app level module which depends on filters, and services
var app = angular.module('app', ['neo4jServices', 
                          'searchModule',
                          'iSearch',
                          'fileupload',
                          'ckeditor',
                          'myApp.filters', 
                          'myApp.services', 
                          'myApp.directives',
                          'myApp.Node',
                          'ui',
                          'ui.bootstrap'
                          ]);

app.config(function($routeProvider, $locationProvider) {
    $locationProvider.html5Mode(true);
    $routeProvider.when('/addNode', {
      templateUrl: '/partials/addNode.html', 
      controller: 'addNodeCtrl'
    });
    $routeProvider.when('/listNodes', {
      templateUrl: '/partials/listNodes.html', 
      controller: 'listNodesCtrl'
    });
    $routeProvider.when('/node/:id', {
      templateUrl: '/partials/Node.html', 
      controller: 'NodeCtrl'
    });
    $routeProvider.otherwise({redirectTo: '/'});
  });
