
app.isearch.factory('isearchquery', function($http, cypherurl, Node) {
  var results = [];

  var findpathquery = {"query": "start node = node({id}), root=node(96) match p = (root)-[*]-node return p",
    "params" : {
    }
  }

  return {
    query: function(val) {
      var starts = "START ";
      var matchs = "MATCH ";
      var returns = "RETURN ";
      val.forEach(function(x, i) {
        starts += "node" + x + "=node" + "(" + x + ")" + ",";
        matchs += "(n)-->" + "(node" + x + ")" + ",";
      });
      starts = starts.slice(0, -1);
      matchs = matchs.slice(0, -1);
      returns += "n";
      var query = starts + " " + matchs + " " + returns;
      return $http.post(cypherurl, {query: query}).success(function(data) {
        data.results = [];
        data.data.forEach(function(x) {
          var node = new Node(x[0]);
          data.results.push(node);
        });
      });
    },
    findpath: function(id) {
      query.params = {id: id};
      return $http.post(cypherurl, query).success(function(data) {
        data.data[0][0].nodes.forEach(function(node) {

        })
      })
    }
  }
});

app.isearch.factory('isearchfacets', function($http, cypherurl, $q) {

  var query = {"query": "start node = node({id}) match n-[r]->node where type(r) = '分类' return n.label, id(n), r.`依据`?",
                "params" : {
                }
              }

  var mmdb = {};
  var root = 96;

  mmdb.getfacets = function(inputs) {
    var defer = $q.defer();
    var rootfacets;
    var inputfacets = {};
    var promises = [];

    promises.push(this.fetchFacetsById(root).success(function(data) {
      rootfacets = data.facets;
      console.log(rootfacets);
    }));

    inputs.forEach(function(input) {
      var that = this;
      promises.push(this.fetchFacetsById(input.id).success(function(data) {
        inputfacets[input.id] = data.facets;
      }));
    }, this);

    var promise = $q.all(promises);
    promise.then(function() {
      console.log(inputfacets);
      Object.keys(inputfacets).forEach(function(input) {
        Object.keys(inputfacets[input]).forEach(function(inputf) {
          Object.keys(rootfacets).forEach(function(root) {
            if (root == inputf) {
              rootfacets[root] = inputfacets[input][inputf];
              rootfacets[root].path = 'has path';
            }
//            else {
//              rootfacets[root] = inputfacets[input][inputf];
//            }
          })
        })
      })
      defer.resolve(rootfacets);
    });

    return defer.promise;
  }

  mmdb.fetchFacetsById = function(id) {
    query.params = {id: id};
    return $http.post(cypherurl, query).success(function(data) {
      var facets = {};
      data.data.forEach(function(x) {
        facets[x[2]] = facets[x[2]] || [];
        facets[x[2]].push({text: x[0], id: x[1]});
      });
      data.facets = facets;
    });
  }
  return mmdb;
});

