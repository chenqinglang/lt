app.isearch.controller('isearchCtrl', function($scope, isearchfacets) {
  $scope.inputs = [];

  $scope.fetchFacets = function() {
    var promise = isearchfacets.getfacets($scope.inputs);
    promise.then(function(facets) {
      $scope.facets = facets;
    });
  }

  $scope.fetchFacets();

  this.addfacet = function(facet) {
    var exist = $scope.inputs.some(function(x) {
      return x.id == facet.id;
    })
    if (!exist) {
      $scope.inputs.push(facet);
      $scope.fetchFacets();
    }
  };
});