app.isearch = angular.module('iSearch', ['neo4jServices', 'myApp.services']);

app.isearch.directive('isearch', function($parse, Index, Utility, isearchquery) {

  return {
    restrict: 'A',
    replace: true,
    controller: 'isearchCtrl',
    templateUrl: 'js/isearch/isearch-template.html',
    link: function(scope, element, attrs, controller) {
      scope.inputs = [];
      scope.aacc = 3;
      var inputelement = element.find('#input');
      inputelement.bind("change", function() {
        scope.$apply(function() {
          scope.inputs = inputelement.select2("data");
          scope.fetchFacets();
          if (scope.inputs.length == 0) return;

          // populate the results from neo4j!
          isearchquery.query(inputelement.select2("val")).then(function(data) {
            scope.results = data.data.results;
          });
        });
      });

      scope.$watch('inputs', function(newval, oldval) {
        if(newval === oldval) {
          return;
        }
        inputelement.select2('data', newval);
      }, true);

      inputelement.select2({
        allowClear:true,
        multiple:true,
        query: function (query) {
          if (query.term == '') {
            query.callback({results: []});
            return;
          }
          Utility.searchnodebylabel(query);
        }
      });
    }
  }
});

app.isearch.directive('facetlink', function() {
  return {
    restrict: 'A',
    require: '^isearch',
    template: '<a href="">{{v.text}}</a>',
    link: function(scope, element, attrs, controller) {
      var facet = scope.v;
      element.bind('click', function() {
        controller.addfacet(facet);
      });
    }
  }
});

app.isearch.directive('facetpath', function() {
  return {
    restrict: 'A',
    template: '{{val | json}}',
    link: function(scope, element, attrs, controllers) {
      scope.$watch('inputs', function() {

      }, true)
    }
  }
});