'use strict';

/* Filters */

angular.module('myApp.filters', []).
  filter('interpolate', ['version', function(version) {
    return function(text) {
      return String(text).replace(/\%VERSION\%/mg, version);
    }
  }])
  .filter('mmdbPath', function() {
    return function(nodeid) {
      return 'node/' + nodeid;
    }
  })
  .filter('selectlabel', function() {
    return function(properties) {
      var label;
      properties.forEach(function(item) {
        if (item.key == "label") {
          label = item.value;
        }
      })
      if (label) {
        return label;
      } else {
        return '未命名';
      }
    }
  })
  .filter('rfilter', function(Utility) {
    return function(relationships, exp) {
      return relationships.filter(function(x) {
        switch (exp.key) {
          case 'out':
            return x.start == exp.node.self;
            break;
          case 'in':
            return x.end == exp.node.self;
            break;
        }
      });
    }
  })
  .filter('createlink', function(Utility) {
    return function(nlink) {
      return Utility.createpathwithid(Utility.extractid(nlink));
    }
  })
  .filter('arrayvalue', function(uploadpath) {
    return function(value) {
      return uploadpath + value;
    }
  });
