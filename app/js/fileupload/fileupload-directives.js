app.fileupload = angular.module('fileupload',[]);

app.fileupload.directive('fileupload', function() {
  return {
    restrict: "A",
    replace: true,
    scope:{
      selectedfiles: "="
    },
    link: function(scope, element, attrs) {
      scope.uploader = new qq.FineUploader({
        element: element[0],
        request: {
          endpoint: '/upload/receiver'
        },
        text: {
          uploadButton: '<div><i class="icon-upload icon-white"></i>添加文件</div>'
        },
        template: '<div class="qq-uploader">' +
                    '<pre class="qq-upload-drop-area span12"><span>{dragZoneText}</span></pre>' +
                    '<div class="qq-upload-button btn btn-success" >{uploadButtonText}</div>' +
                    '<span class="qq-drop-processing"><span>{dropProcessingText}</span><span class="qq-drop-processing-spinner"></span></span>' +
                    '<ul class="qq-upload-list" style="margin-top: 10px; text-align: center;"></ul>' +
                  '</div>',
        classes: {
          success: 'alert alert-success',
          fail: 'alert alert-error'
        },
        deleteFile: {
            enabled: true,
            forceConfirm: true,
            endpoint: '/upload/receiver'
        },
        callbacks: {
          onComplete: function(id, fileName, responseJSON) {
            scope.selectedfiles.push(this.getUuid(id));
          },
          onDelete: function(id) {
            scope.selectedfiles.splice(scope.selectedfiles.indexOf(this.getUuid(id)), 1);
          }
        }
      });
    }
  }
});
