'use strict';

/* Controllers */

app.controller('addNodeCtrl', function($scope, Node, $location, $q) {
	$scope.save = function() {
    var defered = $q.defer();
    var node = Node.create($scope.label, defered);
    defered.promise.then(function() {
      // Navigate to the newly created resource
      $location.path('/node/' + node.id);
    })
  };
});

app.controller('listNodesCtrl', function($scope, $compile) {
  
});


app.controller("NodeCtrl", function ($scope, $routeParams, Node, $location, $q, $dialog) {
  $scope.node = Node.getById($routeParams.id);

  $scope.saveproperty = function(key, value) {
    $scope.isaddpropertyopen = false;
    $scope.node.addProperty(key, value);
  }

  $scope.delproperty = function(key) {
    $scope.node.delProperty(key);
  }

  $scope.saverelationship = function(to, type) {
    $scope.isaddrelationshipopen = false;
    $scope.node.addRelationship(to, type);
  }

  $scope.showaddproperty = function() {
    $scope.isaddpropertyopen = true;
  };

  $scope.canceladdproperty = function() {
    $scope.isaddpropertyopen = false;
  }

  $scope.showaddrelationship = function() {
    $scope.isaddrelationshipopen = true;
  };

  $scope.canceladdrelationship = function() {
    $scope.isaddrelationshipopen = false;
  };

  $scope.openMessageBox = function(){
    var title = '删除此内容';
    var msg = '是否要删除内容与其关联？';
    var btns = [{result:'cancel', label: '取消'}, {result:'ok', label: '删除', cssClass: 'btn-primary'}];

    $dialog.messageBox(title, msg, btns)
        .open()
        .then(function(result){
          if (result == 'ok') {
            $scope.deletenode();
          }
        });
  };

  $scope.deletenode = function() {
    $scope.node.delete();
    $location.path('/');
  }

  $scope.deleterelationship = function(relationship) {
    $scope.node.deleteRelationship(relationship);
  }
});

app.controller("RelationshipCtrl", function($scope) {
  
  $scope.showaddproperty = function() {
    $scope.isaddpropertyopen = true;
  };
  
  $scope.saveproperty = function(key, value) {
    $scope.isaddpropertyopen = false;
    $scope.relationship.updateProperty(key, value);
  }

  $scope.delproperty = function(key) {
    $scope.relationship.delProperty(key);
  }

  $scope.cancel = function(key) {
    $scope.isaddpropertyopen = false;
  }
});
