'use strict';

/* Directives */

angular.module('myApp.directives', []).
  directive('appVersion', ['version', function(version) {
    return function(scope, elm, attrs) {
      elm.text(version);
    };
  }]);

angular.module('searchModule', ['neo4jServices', 'myApp.services']).
  directive('search', function($parse, Index, Utility) {
    return {
      restrict: 'A',
      replace: true,
      templateUrl: 'partials/search.html',
      link: function(scope, element, attrs, controller) {
        scope.$watch(attrs.search, function(value) {
          Index.search({query: "label:" + "*" + $parse(attrs.search)(scope) + "*"}, function(data) {
            scope.results = data.map(function(x) {
              return {label:x.data.label, id:Utility.extractid(x.self)};
            });
          });
        });
      }
    }
  }).
  directive('addProperty', function(richtextkey) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        saveproperty: "&",
        cancel: "&"
      },
      templateUrl: '/partials/addProperty.html',
      link: function(scope, element, attrs) {
        scope.selectedfiles = [];
        scope.propertytype = "text";
        scope._saveproperty = function() {
          switch (scope.propertytype)
          {
            case 'text':
              scope.saveproperty({key: scope.key, value: scope.text});
              break;
            case 'richtext':
              scope.saveproperty({key: scope.key, value: richtextkey + encodeURIComponent(scope.richtext)});
              break;
            case 'file':
              scope.saveproperty({key: scope.key, value: scope.selectedfiles});
              break;
          }
        }
        element.find('.save').first().bind('click', function() {
          // alert('clicked');
        })
      }
    }
  }).
  directive('property', function($compile, $parse, richtextkey) {
    var childScope;
    var editPropertyTemplate = '<div edit-property class="well"></div>';

    return {
      restrict: "A",
      replace: true,
      scope: {
        key: "@",
        value: "=",
        saveproperty: "&",
        delproperty: "&"
      },
      templateUrl: '/partials/property.html',
      link: function(scope, element, attrs, ctrl) {
        scope.valuein = scope.value;
        if (scope.valuein.indexOf(richtextkey) != -1) {
          scope.propertytype = 'richtext';
          scope.valuein = decodeURIComponent(scope.valuein.substring(16));
        }
        else if (scope.valuein instanceof Array) {
          scope.propertytype = 'file';
        }
        else {
          scope.propertytype = 'text';
        }
//        scope.iseditpropertyopen = false;
//        scope.showpropertyedit = function() {
//          scope.iseditpropertyopen = true;
//          var editPropertyElement = angular.element(editPropertyTemplate);
//          $compile(editPropertyElement)(scope);
//          element.find('dd').append(editPropertyElement);
//        };
//        scope.$on('$editPropertyUnload', function() {
//          scope.iseditpropertyopen = false;
//        });
      }
    }
  }).
  directive('contenteditable', function() {
    return {
      restrict: 'A',
      require: '?ngModel',
      link: function(scope, element, attrs, ngModel) {
        if(!ngModel) return; // do nothing if no ng-model

        // Specify how UI should be updated
        ngModel.$render = function() {
          element.html(ngModel.$viewValue || '');
        };

        // Listen for change events to enable binding
        element.bind('keyup change', function() {
          scope.$apply(read);
        });
        element.bind('blur', function() {
          scope.$apply(read);
          if (scope.propertytype == 'richtext') {
            scope.saveproperty({key: scope.key, value:scope.valuein});
          }
        });
        element.bind('keydown', function(event) {
          if (event.keyCode == "13" && scope.propertytype == 'text') {
            event.preventDefault();
          }
        });

        // Write data to the model
        function read() {
          ngModel.$setViewValue(element.html());
        }
      }
    }
  }).
  directive('editProperty', function() {
    return {
      restrict: 'A',
      replace: true,
      templateUrl: '/partials/editProperty.html',
      link: function(scope, element, attrs, controller) {
        // Remove selt when user click Cancel
        scope.cancel = function() {
          element.remove();
          scope.$emit('$editPropertyUnload');
        };

        scope.save = function() {
          scope.saveproperty({key: scope.key, value:scope.valuein});
          element.remove();
          scope.$emit('$editPropertyUnload');
        };
      }
    }
  }).
  directive('addRelationship', function(Node, $q, Utility, Index) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        saverelationship: "&",
        cancel: "&"
      },
      templateUrl: '/partials/addRelationship.html',
      link: function(scope) {
        var defered = $q.defer();
        defered.promise.then(function(data) { 
          $("#relationshiptype").select2({
            allowClear:true,
            multiple:false,
            data: data,
            createSearchChoice:function(term, data) {
            if ($(data).filter(function() {
              return this.text.localeCompare(term)===0; 
              }).length===0) {
                return {id:term, text:term};
              }
            }
          });
        });
        Utility.getrelationshipstypes(defered);

        $("#relationshipto").select2({
          allowClear:true,
          multiple:true,
          query: function (query) {
            if (query.term == '') {
              query.callback({results: []});
              return;
            }
            Utility.searchnodebylabel(query);
          },
          createSearchChoice:function(term, data) {
          if ($(data).filter(function() {
            return this.text.localeCompare(term)===0; 
            }).length===0) {
              return {id:term, text:term};
            }
          }
        });

        scope.save = function() {
          $('#relationshipto').select2('data').forEach(function(x) {
            scope.saverelationship({to:x.nlink, type:$('#relationshiptype').select2('data').text});
          })
          scope.isaddrelationshipopen = false;
        }
      }
    }
  }).
  directive('relationship', function() {
    return {
      restrict: "A",
      replace: true,
      scope: {
        relationship: "=",
        deleterelationship: "&"
      },
      controller: 'RelationshipCtrl',
      templateUrl: '/partials/relationship.html',
      link: function(scope, element, attrs) {

      }
    }
  });